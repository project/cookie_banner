Cookie Banner
-------------
This module allows a site to be compliant with the EU directive on privacy and electronic communications (for more information please visit http://en.wikipedia.org/wiki/Directive_on_Privacy_and_Electronic_Communications).
By displaying this cookie banner on your site you will demonstrate that you are complying with the aforementioned directive, and information about your cookie policy will be readily available to users thanks to the sticky popup window at the bottom.

Features
--------
- Customisable notification settings for cookie notice and the link to the page about your cookie policy. 
- Information text and link can easily be changed by the admin of the site.
- Javascript code that which conditionally sets cookies.
- Multilingual and RTL support.
- Responsive.

Sponsor
-------
The module was initially developed for the British Council (http://www.britishcouncil.org).

Authors
-------
Originally developed by

- Vincenzo Russo (https://www.drupal.org/u/vincenzo)
- James Hopkins (https://www.drupal.org/user/761726)
- Bertan Atalay (@2creative)
- Gary Hawes (https://www.drupal.org/u/GaxZE)

